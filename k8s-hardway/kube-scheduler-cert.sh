#!/bin/sh -x
mkdir -p /root/kube-scheduler-cert
cd /root/kube-scheduler-cert
cat > kube-scheduler-csr.json <<EOF
{
  "CN": "system:kube-scheduler",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "VN",
      "ST": "TEL4VN",
      "L": "HCM",
      "O": "TOKA07",
      "OU": "DarkAngelVN"
    }
  ]
}
EOF

cfssl gencert \
  -ca=/root/ca.pem \
  -ca-key=/root/ca-key.pem \
  -config=/root/ca-config.json \
  -profile=kubernetes \
  kube-scheduler-csr.json | cfssljson -bare kube-scheduler