#!/bin/sh -x
for instance in kube-node-1 kube-node-2 kube-node-3; do
  gcloud compute scp ca.pem /root/client-cert/${instance}-key.pem /root/client-cert/${instance}.pem vagrant@${instance}:~/
done