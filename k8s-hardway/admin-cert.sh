#!/bin/sh -x
mkdir -p /root/admin-cert
cd /root/admin-cert

cat > admin-csr.json <<EOF
{
  "CN": "admin",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "VN",
      "ST": "TEL4VN",
      "L": "HCM",
      "O": "TOKA07",
      "OU": "DarkAngelVN"
    }
  ]
}
EOF

cfssl gencert \
  -ca=/root/ca.pem \
  -ca-key=/root/ca-key.pem \
  -config=/root/ca-config.json \
  -profile=kubernetes \
  admin-csr.json | cfssljson -bare admin