#!/bin/sh -x
for instance in controller-1 controller-2 controller-3; do
  gcloud compute scp ca.pem ca-key.pem /root/api-server-cert/kubernetes-key.pem /root/api-server-cert/kubernetes.pem \
    /root/service-account-cert/service-account-key.pem /root/service-account-cert/service-account.pem vagrant@${instance}:~/
done