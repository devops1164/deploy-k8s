#!/bin/sh -x
#Generate an encryption key and an encryption config suitable for encrypting Kubernetes Secrets
ENCRYPTION_KEY=$(head -c 32 /dev/urandom | base64)

cat > encryption-config.yaml <<EOF
kind: EncryptionConfig
apiVersion: v1
resources:
  - resources:
      - secrets
    providers:
      - aescbc:
          keys:
            - name: key1
              secret: ${ENCRYPTION_KEY}
      - identity: {}
EOF
for instance in controller-1 controller-2 controller-3; do
  gcloud compute scp encryption-config.yaml vagrant@${instance}:~/
done