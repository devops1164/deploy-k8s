#!/bin/sh -x
mkdir -p /root/api-server-cert
cd /root/api-server-cert
#KUBERNETES_PUBLIC_ADDRESS=$(gcloud compute addresses list \
#  --regions $(gcloud config get-value compute/region) \
#  --format 'value(address)' --limit 3)
KUBERNETES_PUBLIC_ADDRESS=35.197.145.191,34.87.19.20,35.185.181.113
KUBERNETES_HOSTNAMES=kubernetes,kubernetes.default,kubernetes.default.svc,kubernetes.default.svc.cluster,kubernetes.svc.cluster.local

cat > kubernetes-csr.json <<EOF
{
  "CN": "kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "VN",
      "ST": "TEL4VN",
      "L": "HCM",
      "O": "TOKA07",
      "OU": "DarkAngelVN"
    }
  ]
}
EOF

cfssl gencert \
  -ca=/root/ca.pem \
  -ca-key=/root/ca-key.pem \
  -config=/root/ca-config.json \
  -hostname=10.32.0.1,10.148.0.2,10.148.0.3,10.148.0.4,${KUBERNETES_PUBLIC_ADDRESS},127.0.0.1,${KUBERNETES_HOSTNAMES} \
  -profile=kubernetes \
  kubernetes-csr.json | cfssljson -bare kubernetes