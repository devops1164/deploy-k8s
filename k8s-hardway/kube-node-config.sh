#!/bin/sh -x
KUBERNETES_PUBLIC_ADDRESS=35.197.145.191,34.87.19.20,35.185.181.113
#Generate a kubeconfig file for each worker node
for instance in kube-node-1 kube-node-2 kube-node-3; do
  kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-credentials system:node:${instance} \
    --client-certificate=/root/client-cert/${instance}.pem \
    --client-key=/root/client-cert/${instance}-key.pem \
    --embed-certs=true \
    --kubeconfig=${instance}.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:node:${instance} \
    --kubeconfig=${instance}.kubeconfig

  kubectl config use-context default --kubeconfig=${instance}.kubeconfig
done

#Generate a kubeconfig file for the kube-proxy service
kubectl config set-cluster kubernetes-the-hard-way \
    --certificate-authority=ca.pem \
    --embed-certs=true \
    --server=https://${KUBERNETES_PUBLIC_ADDRESS}:6443 \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config set-credentials system:kube-proxy \
    --client-certificate=/root/kube-proxy-cert/kube-proxy.pem \
    --client-key=/root/kube-proxy-cert/kube-proxy-key.pem \
    --embed-certs=true \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config set-context default \
    --cluster=kubernetes-the-hard-way \
    --user=system:kube-proxy \
    --kubeconfig=kube-proxy.kubeconfig

  kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig

#Copy the appropriate kubelet and kube-proxy kubeconfig files to each worker instance
for instance in kube-node-1 kube-node-2 kube-node-3; do
  gcloud compute scp ${instance}.kubeconfig kube-proxy.kubeconfig vagrant@${instance}:~/
done