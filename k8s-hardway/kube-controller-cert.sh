#!/bin/sh -x
mkdir -p /root/kube-controller-cert
cd /root/kube-controller-cert
cat > kube-controller-manager-csr.json <<EOF
{
  "CN": "system:kube-controller-manager",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "VN",
      "ST": "TEL4VN",
      "L": "HCM",
      "O": "TOKA07",
      "OU": "DarkAngelVN"
    }
  ]
}
EOF

cfssl gencert \
  -ca=/root/ca.pem \
  -ca-key=/root/ca-key.pem \
  -config=/root/ca-config.json \
  -profile=kubernetes \
  kube-controller-manager-csr.json | cfssljson -bare kube-controller-manager