from pulumi import ComponentResource, ResourceOptions
from pulumi_gcp import compute

def masteraddr01():
    instance_addr = compute.address.Address("master-addr-01")
    return instance_addr
master_addr01 = masteraddr01()

def masteraddr02():
    instance_addr = compute.address.Address("master-addr-02")
    return instance_addr
master_addr02 = masteraddr02()

def masteraddr03():
    instance_addr = compute.address.Address("master-addr-03")
    return instance_addr
master_addr03 = masteraddr03()

def node01_addr():
    instance_addr = compute.address.Address("node01-addr")
    return instance_addr
node01addr = node01_addr()

def node02_addr():
    instance_addr = compute.address.Address("node02-addr")
    return instance_addr
node02addr = node02_addr()

def node03_addr():
    instance_addr = compute.address.Address("node03-addr")
    return instance_addr
node03addr = node03_addr()

def vpc_network():
    network = compute.Network("hardway-network", name = "hardway-network", auto_create_subnetworks=True,)
    return network
pvc_hardway = vpc_network()

compute_firewall = compute.Firewall(
    "firewall",
    network=pvc_hardway.self_link,
    allows=[
        compute.FirewallAllowArgs(
            protocol="icmp",
        ),
        compute.FirewallAllowArgs(
            protocol="udp",
            ports=["8285", "8472"]
        ),
        compute.FirewallAllowArgs(
        protocol="tcp",
        ports=["22", "80", "2379", "2380", "6443", "443", "8080", "10250", "10256"],
    )]
)
