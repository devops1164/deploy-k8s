import pulumi
from pulumi import ResourceOptions
from pulumi_gcp import compute
from pulumi_gcp.compute.instance import Instance
from network_instance import *

#Init OS instance
#script = """#!/bin/bash -x
#sudo -i
#curl -sfL https://gitlab.com/devops1164/deploy-k8s/-/raw/main/k8s-bashscript-gce/k8s_init.sh?inline=false | sh -
#"""

#Create master machine
master01_instance = compute.Instance( "controller-1",
    name="controller-1",
    machine_type="e2-standard-2",
    boot_disk=compute.InstanceBootDiskArgs(
        initialize_params=compute.InstanceBootDiskInitializeParamsArgs(
            image="rocky-linux-cloud/rocky-linux-8-v20210817"
        )
    ),
    network_interfaces=[compute.InstanceNetworkInterfaceArgs(
            network=pvc_hardway.id,
            network_ip="10.148.0.2",
            access_configs=[compute.InstanceNetworkInterfaceAccessConfigArgs(
                nat_ip=master_addr01.address,
            )],
    )],
    service_account=compute.InstanceServiceAccountArgs(
        scopes=["https://www.googleapis.com/auth/cloud-platform"],
    ),
    opts=ResourceOptions(depends_on=[compute_firewall]),
)
pulumi.export("controller-1_name", master01_instance.name)
pulumi.export("controller-1_network", master_addr01.address)

master02_instance = compute.Instance( "controller-2",
    name="controller-2",
    machine_type="e2-standard-2",
    boot_disk=compute.InstanceBootDiskArgs(
        initialize_params=compute.InstanceBootDiskInitializeParamsArgs(
            image="rocky-linux-cloud/rocky-linux-8-v20210817"
        )
    ),
    network_interfaces=[compute.InstanceNetworkInterfaceArgs(
            network=pvc_hardway.id,
            network_ip="10.148.0.3",
            access_configs=[compute.InstanceNetworkInterfaceAccessConfigArgs(
                nat_ip=master_addr02.address,
            )],
    )],
    service_account=compute.InstanceServiceAccountArgs(
        scopes=["https://www.googleapis.com/auth/cloud-platform"],
    ),
    opts=ResourceOptions(depends_on=[compute_firewall]),
)
pulumi.export("controller-2_name", master02_instance.name)
pulumi.export("controller-2_network", master_addr02.address)

master03_instance = compute.Instance( "controller-3",
    name="controller-3",
    machine_type="e2-standard-2",
    boot_disk=compute.InstanceBootDiskArgs(
        initialize_params=compute.InstanceBootDiskInitializeParamsArgs(
            image="rocky-linux-cloud/rocky-linux-8-v20210817"
        )
    ),
    network_interfaces=[compute.InstanceNetworkInterfaceArgs(
            network=pvc_hardway.id,
            network_ip="10.148.0.4",
            access_configs=[compute.InstanceNetworkInterfaceAccessConfigArgs(
                nat_ip=master_addr03.address,
            )],
    )],
    service_account=compute.InstanceServiceAccountArgs(
        scopes=["https://www.googleapis.com/auth/cloud-platform"],
    ),
    opts=ResourceOptions(depends_on=[compute_firewall]),
)
pulumi.export("controller-3_name", master03_instance.name)
pulumi.export("controller-3_network", master_addr03.address)

#Create node01 machine
node01_instance = compute.Instance(
        "kube-node-1",
        name="kube-node-1",
        machine_type="e2-standard-2",
        boot_disk=compute.InstanceBootDiskArgs(
            initialize_params=compute.InstanceBootDiskInitializeParamsArgs(
                image="rocky-linux-cloud/rocky-linux-8-v20210817"
            )
        ),
        network_interfaces=[compute.InstanceNetworkInterfaceArgs(
            network=pvc_hardway.id,
            network_ip="10.148.0.5",
            access_configs=[compute.InstanceNetworkInterfaceAccessConfigArgs(
                nat_ip=node01addr.address,
            )],
        )],
        service_account=compute.InstanceServiceAccountArgs(
        scopes=["https://www.googleapis.com/auth/cloud-platform"],
    ),
    opts=ResourceOptions(depends_on=[compute_firewall]),
)
pulumi.export("node01_name", node01_instance.name)
pulumi.export("node01_network", node01addr.address)
#Create node02 machine
node02_instance = compute.Instance(
        "kube-node-2",
        name="kube-node-2",
        machine_type="e2-standard-2",
        boot_disk=compute.InstanceBootDiskArgs(
            initialize_params=compute.InstanceBootDiskInitializeParamsArgs(
                image="rocky-linux-cloud/rocky-linux-8-v20210817"
            )
        ),
        network_interfaces=[compute.InstanceNetworkInterfaceArgs(
            network=pvc_hardway.id,
            network_ip="10.148.0.6",
            access_configs=[compute.InstanceNetworkInterfaceAccessConfigArgs(
                nat_ip=node02addr.address,
            )],
        )],
        service_account=compute.InstanceServiceAccountArgs(
        scopes=["https://www.googleapis.com/auth/cloud-platform"],
    ),
    opts=ResourceOptions(depends_on=[compute_firewall]),
)
pulumi.export("node02_name", node02_instance.name)
pulumi.export("node02_network", node02addr.address)

#Create node03 machine
node03_instance = compute.Instance(
        "kube-node-3",
        name="kube-node-3",
        machine_type="e2-standard-2",
        boot_disk=compute.InstanceBootDiskArgs(
            initialize_params=compute.InstanceBootDiskInitializeParamsArgs(
                image="rocky-linux-cloud/rocky-linux-8-v20210817"
            )
        ),
        network_interfaces=[compute.InstanceNetworkInterfaceArgs(
            network=pvc_hardway.id,
            network_ip="10.148.0.7",
            access_configs=[compute.InstanceNetworkInterfaceAccessConfigArgs(
                nat_ip=node03addr.address,
            )],
        )],
        service_account=compute.InstanceServiceAccountArgs(
        scopes=["https://www.googleapis.com/auth/cloud-platform"],
    ),
    opts=ResourceOptions(depends_on=[compute_firewall]),
)
pulumi.export("node03_name", node03_instance.name)
pulumi.export("node03_network", node03addr.address)