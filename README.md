Kuberneter Documents:
- https://kubernetes.io/
- https://thecodecloud.in/kubernetes-tutorial-advanced-overview-of-k8s/
- https://phoenixnap.com/kb/understanding-kubernetes-architecture-diagrams

Kubernetes YAML Reference:
- https://yaml.org/
- https://kubernetes.io/docs/reference/kubernetes-api/

Note:
- The every key in yaml file will represent and what does it mean is via kubectl explain command:
    - kubectl explain deploy.spec (or other kind: pod, ingress... )
    - kubectl explain deploy --recursive > deployment_spec.txt (To see full list could be)

- The roadmap for who are interested to devops engineer or SRE:
    - https://roadmap.sh/devops