Cert Manager
- https://cert-manager.io/docs/
- https://joostvdg.github.io/certificates/lets-encrypt-k8s/
- https://www.fosstechnix.com/kubernetes-nginx-ingress-controller-letsencrypt-cert-managertls/

Create LetsEncrypt CA:
- Intall Certbot
- `#DOMAIN=yourdomain`
- `#WILDCARD=*.$DOMAIN`
- `#certbot --manual --preferred-challenges dns certonly -d $DOMAIN -d $WILDCARD`
- Create DNS record of TXT type: _acme-challenge.DOMAIN   TXT   [_acme-challenge_token]

Create SSL Cert in K8S without Cert-Manager:
- kubectl create secret tls [your-cert-name] --cert=/path/to/letsencrypt/fullchain.pem --key==/path/to/letsencrypt/privkey.pem
