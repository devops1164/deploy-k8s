About Flannel-CNI
- https://mvallim.github.io/kubernetes-under-the-hood/documentation/kube-flannel.html
- https://rancher.com/blog/2019/2019-03-21-comparing-kubernetes-cni-providers-flannel-calico-canal-and-weave/
- https://github.com/flannel-io/flannel
- https://medium.com/@ahmetensar/kubernetes-network-plugins-abfd7a1d7cac

Intalling Flannel Plugin:
- kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

Apply configmap
- kubectl apply -f https://gitlab.com/devops1164/deploy-k8s/-/raw/main/yaml-template/Flannel-CNI/flannel-configmap.yaml

Note:
- in net-conf.json block, network is your pod-network-cidr when kubeadm --pod-network-cidr
