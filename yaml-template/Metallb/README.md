About Metallb
- https://metallb.universe.tf/
- https://devopstales.github.io/kubernetes/k8s-metallb/

Install Metallb as layer 2 network
- kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/namespace.yaml
- kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.10.2/manifests/metallb.yaml

Apply config: 
- kubectl apply -f https://gitlab.com/devops1164/deploy-k8s/-/raw/main/yaml-template/Metallb/metallb-config.yaml

Note:
- In data.config block, address is ip range of host network  
