About ArgoCD
- https://argo-cd.readthedocs.io/en/stable/getting_started/

Install ArgoCD
- kubectl create namespace argocd
- kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

Access The Argo CD API Server with TLS by Cert-Manager (require install Cert-Manager first):
- kubectl apply -f letsencrypt-cert.yaml
- kubectl apply -f argocd-ingress.yaml

Note:
- Please change string argocd.beonetech.vn by your domain.
- View password of admin user: kubectl get secret/argocd-initial-admin-secret -n argocd -o jsonpath="{.data.password}" | echo $(base64 --decode)