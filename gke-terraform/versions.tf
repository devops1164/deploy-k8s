variable "service_account_key" {
  type    = string
  default = "linh-324413-371ec54a7bed.json"
}

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.52.0"
    }
  }

  required_version = ">= 0.14"
}

provider "google" {
  credentials = file(var.service_account_key)

  project = var.project_id
  region  = var.region
}